#!/bin/bash
# Author: Salman Ahmad Nurhoiriza
# NPM: 1806204890

HOME_DIRS="user-home-dirs.txt"

`getent passwd | cut -d: -f6 | grep '/home/' > $HOME_DIRS`

while IFS= read -r line
do
	TAR_NAME=`echo "$line" | cut -d'/' -f 3`
		
	if [ -d "$line" ]
	then
		`sudo tar -czvpf /var/backups/$TAR_NAME.bak.tar.gz $line`
	fi
done < "$HOME_DIRS"

`rm $HOME_DIRS`

echo "Backup complete!"
