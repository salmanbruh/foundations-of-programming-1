from turtle import *        
import random               
import math                 

#Turtle Modifications
title("Colorful Chessboard and Flowers")    
speed(0)                                    
getscreen()                                 
colormode(255)                              
hideturtle()

#Input
row = int(numinput("Colorful Chessboard and Flowers", "Enter the Number of Rows:", None, 2, 25))                             
squaresize = int(numinput("Colorful Chessboard and Flowers", "Enter the square size (pixels):", None, None))        
petal = int(numinput("Colorful Chessboard and Flowers", "Enter the number of petals of the flower:", None, None))
r = 0       
g = 0       
b = 0       

#Turtle Modifications 2
screensize(row*squaresize*6, row*squaresize*6)      #Sets height and width of turtle screen

#Random color generator
def warna():                        #Pick random number for rgb
    r = random.randint(0, 255)      
    g = random.randint(0, 255)      
    b = random.randint(0, 255)      
    pencolor(r, g, b)               #sets pen color
    fillcolor(r, g, b)              #Sets fill color

#Beautiful flowers
up()                                
if row >= 15:                       #Set position of turtle pen based on number of rows to make the flower
    setpos(0, 250)                  
else:
    setpos(0, 150)
down()                              
for i in range(petal):              #Loop as many as number of petals
    for j in range(1):              #Loop  for 1 perfect petal with colours
        pensize(3)                  
        warna()                     #Sets pen color
        for k in range(2):          #Loop for 1 perfect petal without different colours
            circle(120, 60)         
            left(120)               
    left(360/petal)                

#Colorful Squares
up()
if row >= 15:                                   
    setpos(math.ceil(0 - (row*squaresize/2)), 100)      #Set position of turtle cursor based on number of rows to start the chessboard
else:
    setpos(math.ceil(0 - (row*squaresize/2)), 0)
down()
pensize(1)
for k in range(row):                    #Draws a chessboard based on number of desired rows
    for i in range(row):        	#Draws 1 square with random colour
        warna()                     
        begin_fill()                
        for j in range(4):          
            forward(squaresize)
            right(90)
        end_fill()
        up()
        forward(squaresize)
        down()
    up()
    if row >= 15:                       #Sets position after 1 row of desired squares has been done to make another row of squares below
        setpos(math.ceil(0 - (row*squaresize/2)), math.ceil(100 - squaresize*(k+1)))
    else:
        setpos(math.ceil(0 - (row*squaresize/2)), math.ceil(0 - squaresize*(k+1)))
    down()

#Tulisan
warna()
tulisan = "Colorful Chessboard of "+str(row**2)+" Squares and a Flower of "+str(petal)+" Petals."
up()
if row >= 15:
    setpos(0, math.ceil(100 - (squaresize*row) - 30))       #Sets pen position for tulisan based on number of rows
else:
    setpos(0, math.ceil(0 - (squaresize*row) - 30))
down()
write(tulisan, False, align="center", font=("Times New Roman", 14, "normal"))
