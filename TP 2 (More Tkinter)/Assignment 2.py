#Importing Turtle
import turtle as t

#Turtle Modifications
t.colormode(255)
t.screensize(1000, 1000)

def choose_file():
    """Prompts user to choose file to load via GUI-based modal window."""
    import tkinter
    from tkinter import filedialog
    root_window = tkinter.Tk()
    root_window.withdraw()
    return filedialog.askopenfilename(filetypes = [("Text Document", "*.txt")])

while True:    
    try:
        ''' Selects file to open and read '''
        selected_file = choose_file()
        print ("Selected file: {}".format (selected_file))   
        a = open(selected_file, 'r')
        b = a.readlines()
        break
    except FileNotFoundError:
        ''' In case file is not chosen, then asks user to choose a file '''
        print("Please choose a file!")



def judul_turtle():
    ''' Sets the title of the Turtle window '''
    window_name = ""
    title = b[0].split()
    name = title[:-1]
    npm = title[-1]
    for i in name:
        window_name = window_name + i + ' '
    judul = window_name + "| " + npm
    t.title(judul)

def warna(r, g, b):
    ''' Sets turtle pen color based on user input '''
    r = int(r)
    g = int(g)
    b = int(b)
    t.pencolor(r, g, b)
    
def coordinate(x, y):
    ''' Sets turtle cursor position based on user input '''
    x = int(x)
    y = int(y)
    t.setpos(x, y)
    
def square(sisi):
    ''' Orders turtle to make a square with 'sisi' as length of
        sides of square '''
    for i in range(4):
        t.forward(int(sisi))
        t.left(90)

def rectangle(width, height):
    ''' Orders turtle to make a rectangle with width and height
        based on user input '''
    width = int(width)
    height = int(height)
    for i in range(2):
        t.forward(width)
        t.left(90)
        t.forward(height)
        t.left(90)
        
def circle(rad):
    ''' Orders turtle to make a circle with a specific radius
        based on user input '''
    rad = int(rad)
    t.circle(rad)

def octagon(sisi):
    ''' Orders turtle to make an octagon with the length of the sides
        as 'sisi' '''
    for i in range(8):
        t.forward(int(sisi))
        t.left(360/8)

def hexagon(sisi):
    ''' Orders turtle to make an hexagon with the length of the sides
        as 'sisi' '''
    for i in range(6):
        t.forward(int(sisi))
        t.left(360/6)

def pentagon(sisi):
    ''' Orders turtle to make an pentagon with the length of the sides
        as 'sisi' '''
    for i in range(5):
        t.forward(int(sisi))
        t.left(360/5)

#Runs 'judul_turtle()' to modify turtle window title
judul_turtle()


for i in range(len(b)-1):
    ''' Loops every data recorded on document '''
    try:
        data = b[i+1].split()
        t.penup()
        if data[0] == 'color':
            warna(data[1], data[2], data[3])
        elif data[0] == 'square':
            coordinate(data[1], data[2])
            t.pendown()
            square(data[3])
        elif data[0] == 'rectangle':
            coordinate(data[1], data[2])
            t.pendown()
            rectangle(data[3], data[4])
        elif data[0] == 'circle':
            coordinate(data[1], data[2])
            t.pendown()
            circle(data[3])
        elif data[0] == 'hexagon':
            coordinate(data[1], data[2])
            t.pendown()
            hexagon(data[3])
        elif data[0] == 'octagon':
            coordinate(data[1], data[2])
            t.pendown()
            octagon(data[3])
        elif data[0] == 'pentagon':
            coordinate(data[1], data[2])
            t.pendown()
            pentagon(data[3])
        else:
            ''' In case data[0] is an unrecognised Command '''
            print("Command {} : Data \'{}\' not defined or invalid!".format(i+1, data[0]))
        print("Command {} : {}, valid and Done!".format(i+1, data[0]))
        
    except:
        ''' In case an Error occurs, then print as below '''
        print("Command {} : Error or data invalid!".format(i+1))
    


        

    
    




