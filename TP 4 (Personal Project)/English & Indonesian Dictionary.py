# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 06:50:58 2018

@author: Salman Ahmad N.

p.s Make sure to connect to Internet before running this script
"""
#Basic Importing, imports modules to be used in app
import io
from tkinter import Tk, Frame, Label, W, E, Entry, Button, Radiobutton, RAISED, StringVar, Text, END, messagebox
from PIL import Image, ImageTk
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen

#Class of the app
class Dictionary(Frame):
    #__init__ runs when class is called
    def __init__(self, master):
        '''
        Basically inherits methods from superclass (Frame) and extends it by adding instance variables such as
        self.master. And also sets the title of the Tkinter User Interface and automatically runs self.UI.
        ''' 
        super().__init__(master)
        self.master = master
        self.master.title('Dictionary')
        self.UI()

    #Function that shows a message box when an error occurs.
    def msgbox(self):
        '''
        Usually called when user inputs a word that cannot be found in the online dictionary database
        '''
        messagebox.showerror(title = 'Input Error', message = 'Error 404!\nWord not found!\nPlease enter a new word!')
    
    #Function that creates the text widget that contains the meaning of the words
    def create_text(self, dict_used):
        '''
        First it checks what type of dictionary is used, then according to which dictionary is being used it sets
        a local variable 'kata' which will be used as the heading or basically the word that the user inputted
        to search for the meaning. Then it creates the text widget. Which then followed by the insertion of the 
        variable 'kata' first and then by using the loop to extract all the meanings from the list 'meanings' 
        and inserts all one by one. Because the header or the word inputted has to have a different font size,
        the method to use is the 'tag_add' and 'tag_config' method that can be used for the widget. Finally, it
        packs the word and the meanings into the Frame.
        '''
        if dict_used == 'KBBI':
            kata = self.html.find('h2').text
            kata = f'{kata}\n\n'
        elif dict_used == 'Oxford':
            kata = self.html.find('h2').text
            if kata[-3].isdigit() == True:
                kata = f'{kata[:len(kata)-3]}\n\n'
            else:
                kata = f'{kata[:len(kata)-2]}\n\n'
        teks = Text(self.separator2, font = 'times 20 italic', fg = 'black')
        teks.insert(END, f'{kata}')
        for i in range(len(self.meanings)):
            teks.insert(END, f'{i+1}. ')
            for j in range(len(self.meanings[i])):
                teks.insert(END, self.meanings[i][j])
                teks.insert(END, ' ')
            teks.insert(END, '\n\n')
        teks.tag_add('Normal Char', f'1.{len(kata)}', END)
        teks.tag_config('Normal Char', font = 'times 14')
        teks.pack()
 
    def search_kbbi(self):
        '''
        This fuuction runs when user chooses the KBBI dictionary. First it takes users input and concantinates it to
        the variable 'url' which is the url of the dictionary. Then, using the 'urllib' library to open the url
        and then the module 'beautiful soup' takes the data (html) that structures the web dictionary.
        And from that we find a certain tag of the html data and using the method 'find'. Once found, it then appends
        all the data from the tag and automatically runs the 'create text' function.
        '''
        url = f'https://kbbi.kemdikbud.go.id/entri/{self.word_to_search}'
        data = requests.get(url)
        self.html = BeautifulSoup(data.content, 'html.parser')
        word_containers = self.html.find('ol')
        if word_containers == None:
            word_containers = self.html.find('ul', style="list-style: none;")
        if word_containers == None:
            self.msgbox()
        else:
            meaning = word_containers.find_all('li')
            self.meanings = []
            for i in range(len(meaning)):
                mean = meaning[i].text.split()
                self.meanings.append(mean[1:])
            self.create_text('KBBI')

    def search_oxford(self):
        '''
        Runs the same as the function 'search_kbbi'
        '''
        url = f'https://en.oxforddictionaries.com/definition/{self.word_to_search}'
        data = requests.get(url)
        self.html = BeautifulSoup(data.content, 'html.parser')
        word_containers = self.html.find('ul', class_="semb")
        if word_containers == None:
            self.msgbox()
        else:
            meaning = word_containers.find_all('p')
            self.meanings = []
            for i in range(len(meaning)):
                mean = meaning[i].text[1:].split()
                self.meanings.append(mean)
            self.create_text('Oxford')

    def on_button_click(self):
        '''
        This function runs when the search button is clicked. It first destroys all the widgets that is in the Frame
        'separator2' so that it won't overlap if you search for the 2nd time and so on. Then basically it determines
        what dictionary the users chose and runs the function according to the chosen function.
        '''
        self.word_to_search = self.word.get()
        for child in self.separator2.winfo_children():
            child.destroy()
        chosen_dict = self.chosen_dict.get()
        if chosen_dict == 'KBBI':
            self.search_kbbi()
        elif chosen_dict == 'Oxford':
            self.search_oxford()

    def UI(self):
        '''
        This is basically what constructs the User Interface of the tkinter. two Frames were made.
        '''
        separator = Frame(self.master, height = 400, width = 400)
        separator.pack()
        header = Label(separator,
                       text = 'Welcome to Salman\'s Indo-English Dictionary',
                       font = 'times 15 bold',
                       bg = 'black', fg = 'white',
                       relief = RAISED)
        header.grid(row = 0, column = 0, columnspan = 3)

        label_choose_dict = Label(separator, 
                                  text = 'Online Dictionary to use (select one):', 
                                  font = 'times 13')
        label_choose_dict.grid(row = 1, column = 0, columnspan = 3)
       
        self.chosen_dict = StringVar()
        dicts = ['KBBI', 'Oxford']
        for i in range(len(dicts)):
            rb = Radiobutton(separator, 
                             text = dicts[i], 
                             font = 'times 12', 
                             variable = self.chosen_dict, 
                             value = dicts[i])
            rb.grid(row = 2, column = i, columnspan = 2)

        label_word = Label(separator, 
                           text = 'Word:', 
                           font = 'times 13')
        label_word.grid(row = 3, column = 0)

        self.word = Entry(separator)
        self.word.grid(row = 3, column = 1, sticky = W+E)

        search_img_url = 'http://chittagongit.com//images/search-button-icon-png/search-button-icon-png-1.jpg'
        a = urlopen(search_img_url)
        raw_data = a.read()
        a.close()
        search_img = Image.open(io.BytesIO(raw_data))
        search_img = search_img.resize((25, 25))
        img = ImageTk.PhotoImage(search_img)
        search_button = Button(separator, 
                               image=img, 
                               command = self.on_button_click)
        search_button.image = img
        search_button.grid(row = 3, column = 2)

        self.separator2 = Frame(self.master, 
                                width = 1000, 
                                height = 400, 
                                bg = 'white')
        self.separator2.pack()

def main():
    root = Tk()
    dict = Dictionary(root)
    dict.mainloop()
    
if __name__ == '__main__':
    main()