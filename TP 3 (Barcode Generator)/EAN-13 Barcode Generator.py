from tkinter import *                   #Importing tkinter module
from tkinter import messagebox          #Import method messagebox from module tkinter

class GUI(Frame):       
    def __init__(self, master):         #The method that runs first
        super().__init__(master)        #Inherit methods from superclass Frame
        self.master = master            #Sets Tk() as master or root of widgets
        self.label_entry_canvas()       #Runs label_entry_canvas method within class GUI
        self.master.title('EAN-13 Barcode Generator')
        
    def change_status(self, text):      #Changes the status text of the app
        self.status['text'] = text      #Changes status text to argument text

    def show_info(self, text):          #Show info widget when an action requires attention
        messagebox.showinfo('Info!', message = 'Attention!\n{}'.format(text))        #Show the showinfo widget with a text according to text

    def check_len_digit_and_save_name(self):
        '''
        This method checks the user Entry. If the save name entry is in wrong format or empty, 
        then return a warning that asks user to re-entry the save file name. If the digit Entry is not digit,
        then return a warning that asks the user to re-entry a digit based Entry. If length of digit Entry is not 12,
        then return a warning that asks the user to re-entry a digit that has 12 digits.
        '''
        self.digit_check = True
        self.check_savename = True
        self.save_filename = self.inp_savefile.get()
        self.code = self.inp_code.get()

        if len(self.code) != 12 and (self.save_filename == '' or self.save_filename[-4:]) != '.eps':    #point 
            self.show_info('Code not 12 digits and file name incorrect format.\nPlease enter Input in the correct format')
            if len(self.code) > 12:
                self.inp_code.delete(12, END)
            self.digit_check = False
            self.check_savename = False

        elif self.code.isdigit() == False:
            self.show_info('Code not digits.\nPlease enter a 12 digit code!')
            self.inp_code.delete(0, END)
            self.digit_check = False
            
        elif len(self.code) != 12:
            self.show_info('Code not 12 digits.\nPlease re-enter code!')
            if len(self.code) > 12:
                self.inp_code.delete(12, END)
            self.digit_check = False
            
        elif self.save_filename == '':
            self.show_info('Please enter a save file name!')
            self.check_savename = False
            
        elif self.save_filename[-4:] != '.eps':
            self.show_info('Incorrect save file format.\nPlease re-enter file name in the correct format (*.eps).')
            self.check_savename = False

        if self.check_savename == False or self.digit_check == False:       
            self.change_status('Fail to generate Barcode!')         #Change the status of GUI
                
    def checkdigit(self):
        '''
        This function finds the last digit to make the barcode from the 12 digit code that the users entered.
        First, it adds all the digits with even indexes (0, 2, 4, ..., 10) and multiplies it by 1 and adds
        the result of the calculation to the variable 'self.checksum'. 
        And then, it adds all the digits with odd indexs (1, 3, 5, ..., 11) and multiplies it by 3 and adds
        the result of the calculation to the variable 'self.checksum'.
        It then checks the last digit of the 'self.checksum', if the last digit of checksum is not 0, then
        the check digit will be 10 - <last digit of checksum>, else the check digit is 0. Then assigns the
        result of the check digit to the variable 'self.check_digit'.
        Then it will create a text about the check digit in the canvas.
        '''
        self.checksum = 0
        for digits in self.code[0:12:2]:
            self.checksum += (int(digits)*1)
        for digits in self.code[1:12:2]:
            self.checksum += (int(digits)*3)
        a = self.checksum % 10
        if a != 0:
            self.check_digit = 10 - a
        else:
            self.check_digit = a
        self.code += str(self.check_digit)

        self.canvas.create_text(175, 280,
                                font = 'Times 15 bold',
                                text = 'Check Digit: {}'.format(self.check_digit),
                                fill = 'orange')

    def digits_encode(self):
        '''
        This function is used to encode the digits enterred by user to binary digits which will be used to create the barcode.
        It first creates the list of the structure of the 6 digits in the left side of the barcode except the first digit of the code.
        It then creates the list of L, G, R binary to use to make the barcode.
        First it checks the parity of the structure based on the first digit. Parity_structure will then be used to make the left side of the barcode.
        Then, a list of binaries of the left bars are made, and then a loop to identify which binary to be used according to the digits entered by
        user and the parity structure (L_code or G_code) and appends to list left_bars. 
        It then does the same thing for the right side, except it does not have a parity so it will always use the R_code and adds 
        the binary to the list right_bars.
        '''
        left_structure = ['LLLLLL', 'LLGLGG', 'LLGGLG', 'LLGGGL', 'LGLLGG',
                          'LGGLLG', 'LGGGLL', 'LGLGLG', 'LGLGGL', 'LGGLGL']
        L_code = ['0001101', '0011001', '0010011', '0111101', '0100011',
                  '0110001', '0101111', '0111011', '0110111', '0001011']
        G_code = ['0100111', '0110011', '0011011', '0100001', '0011101',
                  '0111001', '0000101', '0010001', '0001001', '0010111']
        R_code = ['1110010', '1100110', '1101100', '1000010', '1011100',
                  '1001110', '1010000', '1000100', '1001000', '1110100']

        self.first_digit = self.code[0]
        parity_structure = left_structure[int(self.first_digit)]
        
        self.left_bars = []
        for i in range(6):
            digit = self.code[i+1]
            if parity_structure[i] == 'L':
                bar = L_code[int(digit)]
                self.left_bars.append(bar)
            else:
                bar = G_code[int(digit)]
                self.left_bars.append(bar)

        self.right_bars = []
        for i in range(6):
            digit = self.code[i+7]
            bar = R_code[int(digit)]
            self.right_bars.append(bar)

    def header(self):
        '''
        This function basically is just the text above the barcode which is 'EAN-13 Barcode:'
        '''
        self.canvas.create_text(175, 70,
                                font = 'Times 18 bold',
                                text = 'EAN-13 Barcode:',
                                justify = CENTER)

    def one_bar(self, x0, y0, x1, y1, colour):
        '''
        This function is used to make a bar and will be useful for the function barcode. a digit consists of 7 bars.
        '''
        self.canvas.create_rectangle(x0, y0, x1, y1,
                                     fill=colour,
                                     outline='')

    def barcode(self):
        '''
        This is the main program where it will create the barcode based on the encoding of the digits.
        First, it creates a list of bits that will be the start, center, and end markers of the barcode.
        Then concantenates the list of markers with the list binaries that has been encoded.
        Then it make the barcode by looping every index of the bars.
        '''
        start_marker = ['101']
        center_marker = ['01010']
        end_marker = ['101']
        
        self.bars = start_marker + self.left_bars + center_marker + self.right_bars + end_marker

        x0 = 32.5
        y0 = 100
        x1 = 35.5

        for bars in self.bars:
            for i in range(len(bars)):
                if len(bars) != 7:
                    y1 = 240
                    if bars[i] == '1':
                        self.one_bar(x0, y0, x1, y1, 'blue')
                    else:
                        self.one_bar(x0, y0, x1, y1, 'white')
                else:
                    y1 = 235
                    if bars[i] == '1':
                        self.one_bar(x0, y0, x1, y1, 'black')
                    else:
                        self.one_bar(x0, y0, x1, y1, 'white')
                x0 += 3
                x1 += 3

    def barcode_digits(self):
        '''
        This function creates a text in the canvas, the text is the digits of the barcode.
        '''
        digits = self.code[0] + ' ' + self.code[1:7] + ' ' + self.code[7:13]
        x = 22
        y = 250
        for i in range(len(digits)):
            self.canvas.create_text(x, y,
                                    font = 'Times 18 bold',
                                    text = digits[i],
                                    justify = CENTER)
            if i == 1:
                x += 9
            elif i == 8:
                x += 15
            else:
                x += 21

    def save_barcode(self):
        '''
        This function saves the things that is on the canvas as a PostScript (*.eps) file.
        And returns a notif that it has been saved
        '''
        self.canvas.postscript(file = self.save_filename,
                               colormode = 'color',
                               rotate = False)
        self.change_status('Barcode saved to \'{}\'.'.format(self.save_filename))
        self.show_info('Success!\nBarcode saved as \'{}\'.'.format(self.save_filename))

    def bind_to_Enter(self, event):
        '''
        This function is the function that will be run after the <Return> / Enter key has been pressed.
        It will first clear the things that are in the canvas. Then runs the rest of the functions.
        '''
        self.canvas.delete('all')
        self.check_len_digit_and_save_name()
        if self.check_savename == False or self.digit_check == False:
            pass
        else:
            self.checkdigit()
            self.digits_encode()
            self.header()
            self.barcode()
            self.barcode_digits()
            self.save_barcode()
        
    def label_entry_canvas(self):
        '''
        This function is what asseblles the GUI (Label, Entry, and Canvas) and binding of the <Return> key.
        '''
        label_heading1 = Label(self.master,
                               font = 'Times 12 bold',
                               text = 'EAN-13 Barcode Generator',
                               fg = 'white', bg = 'blue',
                               padx = 85)
        label_heading1.pack()

        label_heading2 = Label(self.master,
                               font = 'Times 12 bold',
                               text = 'By: Salman Ahmad Nurhoiriza',
                               fg = 'white', bg = 'red',
                               padx = 76.5)
        label_heading2.pack()
        
        label_savefile = Label(self.master,
                               font = 'Times 11 bold',
                               text = 'Save barcode to PS file (eg: EAN13.eps):')
        label_savefile.pack()
        
        self.inp_savefile = Entry(self.master)
        self.inp_savefile.pack()

        label_code = Label(self.master,
                           font = 'Times 11 bold',
                           text = 'Enter code (first 12 decimal digits):')
        label_code.pack()

        self.inp_code = Entry(self.master)
        self.inp_code.pack()

        self.status = Label(self.master,
                            font = 'Times 10',
                            text = 'Input PS file and Code, then press \'Enter\' key.')
        self.status.pack()
        
        self.canvas = Canvas(self.master,
                        background = 'white',
                        width = 350, height = 350)
        self.canvas.pack()

        self.master.bind('<Return>', self.bind_to_Enter) 

def main():
    root = Tk()
    app = GUI(root)
    root.mainloop()

if __name__ == '__main__':
    main()
        
